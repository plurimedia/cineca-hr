var CONFIG = require(process.cwd() + '/server/config.js'),
    express = require('express');
    path = require('path');
    http = require('http');
    bodyParser = require('body-parser');
    mongoose = require('mongoose');
    mongoose.Promise = require('bluebird');
    session = require('express-session');
    app = express();
    mongoUrl = CONFIG.MONGO;
    cors = require('cors');
    port = CONFIG.PORT;

ElencoConfigurazioniRoutes = require('./server/importazionedati/routes/elencoconfigurazioni.routes');
ElencoCaricamentiRoutes = require('./server/importazionedati/routes/elencocaricamenti.routes');
DettaglioCaricamentiRoutes = require('./server/importazionedati/routes/dettagliocaricamenti.routes');
CrossRoutes = require('./server/importazionedati/routes/cross.routes');

connect = function () {
    var options = {
        socketOptions: {
            keepAlive: 1
        },
        useNewUrlParser: true
    };
    mongoose.connect(mongoUrl, options);
}

// Mongo connection
connect();
mongoose.connection.on('error', function (err) {
    throw err;
});
mongoose.connection.on('disconnected', connect);
mongoose.connection.on('open', function () {
    console.info('connected to ', mongoUrl);
    console.info('ambiente', CONFIG.ENV);
    console.info('config', CONFIG);
});

mongoose.set('debug', CONFIG.DEBUG);

app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: false }));

app.use(express.static(path.join(process.cwd(), 'dist')));
app.use(express.static('client'));

app.use(cors());

const server = http.createServer(app);
app.set('port', port);

app.use('/', ElencoConfigurazioniRoutes);
app.use('/', ElencoCaricamentiRoutes);
app.use('/', DettaglioCaricamentiRoutes);
app.use('/', CrossRoutes);

// LISTEN ON PORT
server.listen(port, () => console.log(`API RUNNING ON PORT: ${port}`)); 