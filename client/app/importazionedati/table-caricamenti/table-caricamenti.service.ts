import { Injectable } from '@angular/core';

import { HttpClient, HttpHeaders, HttpParams } from '@angular/common/http';
import { isUndefined} from 'lodash';

import { Router } from '@angular/router';
import { DataService } from '../shared_services/data.service';

@Injectable({
  providedIn: 'root'
})
export class TableCaricamentiService {

  baseURL;
  

  constructor(
    private http: HttpClient, 
    private router: Router,
    public dataService: DataService) {
      this.baseURL = dataService.baseUrl; // 'http://localhost:3000/api/';
    }

   // Http Headers
   httpOptions = {
    headers: new HttpHeaders({
      'Content-Type': 'application/json'
    })
  }

  getElencoCaricamenti(): any {
    return this.http.get(this.baseURL+"elencocaricamentilista"); 
  }

  getElencoCaricamentiSearch(parameters : any): any {

     let params = new HttpParams();

     // Begin assigning parameters
     if (!isUndefined(parameters)) {
         params = isUndefined(parameters.tipologia) ? params : params.append('tipologia', parameters.tipologia);
         params = isUndefined(parameters.operazione) ? params : params.append('operazione', parameters.operazione);
         params = isUndefined(parameters.start_exec) ? params : params.append('start_exec', parameters.start_exec);
         params = isUndefined(parameters.end_exec) ? params : params.append('end_exec', parameters.end_exec);
         params = isUndefined(parameters.codice) ? params : params.append('codice', parameters.codice);
         params = isUndefined(parameters.descrizione) ? params : params.append('descrizione', parameters.descrizione);
      }

    return this.http.get(this.baseURL+"elencocaricamentilistasearch", { params: params }); 
  }

  getTipologiaList(): any {
   return this.http.get(this.baseURL+"elencoconfigurazionilista")
  }

  gotoNewCaricamento(){
    this.router.navigate(['/new']);
  }

  gotoDettagliCaricamento(){
    this.router.navigate(['/dettagli']);
  }

  deleteElement(data){
    return this.http.put(this.baseURL+"elimina", data, this.httpOptions); 
  }

}
