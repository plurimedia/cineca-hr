import { TestBed } from '@angular/core/testing';

import { TableCaricamentiService } from './table-caricamenti.service';

describe('TableCaricamentiService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: TableCaricamentiService = TestBed.get(TableCaricamentiService);
    expect(service).toBeTruthy();
  });
});
