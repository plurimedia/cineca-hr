import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { TableCaricamentiComponent } from './table-caricamenti.component';

describe('TableCaricamentiComponent', () => {
  let component: TableCaricamentiComponent;
  let fixture: ComponentFixture<TableCaricamentiComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ TableCaricamentiComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(TableCaricamentiComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
