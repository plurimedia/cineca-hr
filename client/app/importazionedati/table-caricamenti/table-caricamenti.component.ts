import { Component, OnInit, ViewChild, Inject } from '@angular/core';
import { FormControl } from '@angular/forms';
import { MatSort } from '@angular/material/sort';
import { MatPaginator } from '@angular/material/paginator';
import { MatTableDataSource } from '@angular/material/table';
import { MatDatepickerInputEvent } from '@angular/material/datepicker';
import { TableCaricamentiService } from './table-caricamenti.service';
import { DataService } from '../shared_services/data.service';
import { MatSnackBar } from '@angular/material/snack-bar';
import { MatDialog, MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { ElencoConfigurazioni } from '../models/elencoconfigurazioni.model'
import { isNil } from 'lodash';


export interface Operazione {
  value: string;
  viewValue: string;
}

export interface CustomDialogData {
  action: string;
  tipologia: string;
  codice: string;
}


@Component({
  selector: 'app-table-caricamenti',
  templateUrl: './table-caricamenti.component.html',
  styleUrls: ['./table-caricamenti.component.css']
})
export class TableCaricamentiComponent implements OnInit {
  dataSource;
  tipologiaList: ElencoConfigurazioni[];
  tipologiaSelected;
  operazioneSelected;
  startDateSelected;
  endDateSelected;
  codiceSelected;
  descrizioneSelected;
  loading;
  cloneRow;
  startDate = new FormControl();

  @ViewChild(MatSort, { static: true }) sort: MatSort;
  @ViewChild(MatPaginator, { static: true }) paginator: MatPaginator;

  displayedColumns: string[] = [
    'tipologia',
    'operazione',
    'codice',
    'descrizione',
    'start_exec',
    'end_exec',
    'cron_exec',
    'totale',
    'totale_ok',
    'totale_ko',
    'status',
    'azioni'];

  listaOperazioni: Operazione[] = [
    { value: 'ins-0', viewValue: 'Inserimento' },
    { value: 'canc-1', viewValue: 'Cancellazione' }
  ];

  constructor(public tableCaricamentiService: TableCaricamentiService,
    public dataService: DataService,
    public dialog: MatDialog,
    private _snackBar: MatSnackBar) {

  }

  ngOnInit() {
    this.getTipologia();
    this.loading = true;
    this.getTable(null);
  }

//popola il filtro Tipologia in base ai dati sul db
  private getTipologia() {
    this.tableCaricamentiService.getTipologiaList().subscribe(
      (data: ElencoConfigurazioni[]) => {
        this.tipologiaList = data;
      }
    );
  }

//popola la tabella principale in base ai dati sul db
  private getTable(isDelete) {

    this.tableCaricamentiService.getElencoCaricamenti().subscribe(
      (data) => {
        this.dataSource = new MatTableDataSource(data);
        this.dataSource.sort = this.sort;
        this.dataSource.paginator = this.paginator;
        !isNil(isDelete)? this.dataSource.paginator.pageIndex = 0 :"";
        this.loading = false;
      }
    );
  }

//memorizzo in una variabile la Start/End Date inserita
  public getDateSelected(type: string, event: MatDatepickerInputEvent<Date>) {
    if (type == 'startDate')
      this.startDateSelected = event.value;
    else
      this.endDateSelected = event.value;
  }

//funzione di ricerca - filtra la tabella principale in base alle condizioni indicate  
  public search() {
    let conditionsObj = {};
    (this.tipologiaSelected) ? conditionsObj["tipologia"] = this.tipologiaSelected : null;
    (this.operazioneSelected) ? conditionsObj["operazione"] = this.operazioneSelected : null;
    (this.startDateSelected) ? conditionsObj["start_exec"] = this.startDateSelected : null;
    (this.endDateSelected) ? conditionsObj["end_exec"] = this.endDateSelected : null;
    (this.codiceSelected) ? conditionsObj["codice"] = this.codiceSelected : null;
    (this.descrizioneSelected) ? conditionsObj["descrizione"] = this.descrizioneSelected : null;

    this.tableCaricamentiService.getElencoCaricamentiSearch(conditionsObj).subscribe(
      (data) => {
        this.dataSource = new MatTableDataSource(data)
        this.dataSource.sort = this.sort;
        this.dataSource.paginator = this.paginator;
      }
    );
  }

//indirizza alla pagina di NuovoCaricamento  
  public goTo() {
    this.tableCaricamentiService.gotoNewCaricamento();
  };

//indirizza alla pagina di DettagliCaricamento
  public goToDettagli(row) {
    this.dataService.data = row;
    this.tableCaricamentiService.gotoDettagliCaricamento();
  }


//dialog di cancellazione
  public openDeleteDialog(selectedRow) {

    const dialogRef = this.dialog.open(DeleteDialog, {
      width: '350px',
      data: { row: selectedRow }
    });

    dialogRef.afterClosed().subscribe(result => {
      //console.log(`The dialog was closed: ${result.event}`);
      this.cloneRow = result.data.row;
      if(`${result.event}` === 'Conferma'){
        this.deleteRow(this.cloneRow );
      }
    });
  }

  public deleteRow(row) {
    console.log(row)
    let isDelete = true;
    this.tableCaricamentiService.deleteElement(row).subscribe(
      (data: any) => {

        if (data.success === true) {
          this.getTable(isDelete);
           this._snackBar.open(data.message, "x", {
            duration: 2000,
          }); 
          console.log("Cancellazione effettuata")    
        }
        else {
          this.getTable(null)
            this._snackBar.open("Error", "x", {
             duration: 2000,
           }); 
          console.log("Problemi durante la cancellazione")
        }
        
      }
    );
  }
}


//componente dialog di cancellazione
@Component({
  selector: 'delete-dialog',
  templateUrl: './delete-dialog.html',
  styleUrls: ['./delete-dialog.css']
})
export class DeleteDialog {
  action : string;
  local_data: any;

  constructor(
    public dialogRef: MatDialogRef<DeleteDialog>,
    @Inject(MAT_DIALOG_DATA) public data: CustomDialogData) { 
      this.local_data = data;
      this.action = this.local_data.action
    }

  onEscClick(): void {
    this.dialogRef.close({event:'Esci'});
  }

  doAction(): void {
    this.dialogRef.close({event:'Conferma', data:this.local_data});
  }

}