export class DettaglioCaricamenti {
    caricamento: string;
    nriga: number
    campi: any;
    esito: string;
    codice_errore: string;
    descrizione_errore: string;
    tenant: string;
}
