export class ElencoCaricamenti {
    tipologia: string;
    operazione: string;
    codice: string;
    descrizione: string;
    start_exec: Date;
    end_exec: Date;
    cron_exec: Date;
    totale: number;
    totale_ok: number;
    totale_ko: number;
    status: string;
    tenant: string;
}
