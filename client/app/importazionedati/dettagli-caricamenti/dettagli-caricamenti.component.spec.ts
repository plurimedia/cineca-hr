import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { DettagliCaricamentiComponent } from './dettagli-caricamenti.component';

describe('DettagliCaricamentiComponent', () => {
  let component: DettagliCaricamentiComponent;
  let fixture: ComponentFixture<DettagliCaricamentiComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ DettagliCaricamentiComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(DettagliCaricamentiComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
