import { TestBed } from '@angular/core/testing';

import { DettagliCaricamentiService } from './dettagli-caricamenti.service';

describe('DettagliCaricamentiService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: DettagliCaricamentiService = TestBed.get(DettagliCaricamentiService);
    expect(service).toBeTruthy();
  });
});
