import { Component, OnInit, ViewChild } from '@angular/core';
import { MatSort } from '@angular/material/sort';
import { MatPaginator } from '@angular/material/paginator';
import { MatTableDataSource } from '@angular/material/table';
import { DettagliCaricamentiService } from './dettagli-caricamenti.service'
import { DataService } from '../shared_services/data.service';
import { isNil, map, get } from 'lodash';
import { saveAs } from 'file-saver';
import { DettaglioCaricamenti } from '../models/dettaglicaricamenti.model'


export interface Visualizzazione {
  value: string;
  viewValue: string;
}

export interface DownloadType {
  value: string;
  viewValue: string;
}

@Component({
  selector: 'app-dettagli-caricamenti',
  templateUrl: './dettagli-caricamenti.component.html',
  styleUrls: ['./dettagli-caricamenti.component.css']
})
export class DettagliCaricamentiComponent implements OnInit {
  dataParent;
  dataSource;
  visualizzazioneSelected;
  dynamicColumns: string[] = [];
  displayedColumns: string[] = [];
  filtervar = "";

  listaVisualizzazioni: Visualizzazione[] = [
    { value: 'all-0', viewValue: 'Visualizza tutte le righe' },
    { value: 'ok-1', viewValue: 'Visualizza righe esito ok' },
    { value: 'ko-2', viewValue: 'Visualizza righe esito ko' }
  ];

  listaDownloadType: DownloadType[] = [
    { value: 'originale-0', viewValue: 'File originale' },
    { value: 'all-1', viewValue: 'Con esito riga' },
    { value: 'visualizzate-2', viewValue: 'Scarica solo righe visualizzate' }
  ];


  @ViewChild(MatSort, { static: true }) sort: MatSort;
  @ViewChild(MatPaginator, { static: true }) paginator: MatPaginator;

  constructor(public dataService: DataService,
    public dettagliCaricamentiService: DettagliCaricamentiService) {

  }

  ngOnInit() {
    if (!isNil(this.dataService.data)) {
      localStorage.removeItem('dataDettagliCaricamenti');
      localStorage.setItem('dataDettagliCaricamenti', JSON.stringify(this.dataService.data));
      this.dataParent = this.dataService.data;
    }
    else {
      this.dataParent = JSON.parse(localStorage.getItem('dataDettagliCaricamenti'));
    }
    this.getTableDettagli()
  }

  //recupera i dati dal server e li mostra nella tabella dettagli
  private getTableDettagli() {

    this.dettagliCaricamentiService.getElencoDettagliSingoloCaricamento(this.dataParent._id)
      .subscribe(
        (data) => {
          this.displayedColumns.push('nriga')
          let firstRowHeader = ((Object.values(data)[0]) as DettaglioCaricamenti);
          this.dynamicColumns = Object.keys(firstRowHeader.campi);
          this.displayedColumns = this.displayedColumns.concat(this.dynamicColumns);
          this.displayedColumns.push('esito')
          this.displayedColumns.push('cod_err')
          this.displayedColumns.push('desc_err')
          this.dataSource = new MatTableDataSource<Object>(data)
          this.dataSource.sort = this.sort;
          this.dataSource.paginator = this.paginator;
        }
      );
  }

  //filtro su visualizzazione tabella
  public displayRows() {
    switch (this.visualizzazioneSelected) {
      case "ok-1": {
        this.dataSource.filter = "ok";
        this.filtervar = "ok";
        break;
      }
      case "ko-2": {
        this.dataSource.filter = "ko";
        this.filtervar = "ko";
        break;
      }
      default: {
        this.dataSource.filter = "";
        this.filtervar = "";
        break;
      }
    }
  }

  //scelta download del file csv
  public downloadFile(type: any) {
    let data = this.dataSource.data
    let header;
    let csv;

    switch (type) {
      case "originale-0":
        header = this.displayedColumns.filter(x => x != 'esito' && x != 'cod_err' && x != 'desc_err' && x != 'nriga');
        csv = data.map(row => header.map(fieldName =>
            isNil(row.campi[fieldName])? '':row.campi[fieldName]
        ).join(','));
        break;
      case ("visualizzate-2"):
        if (this.filtervar != "") {
          header = this.displayedColumns;
          data = data.filter(x => x['esito'] === this.filtervar)
   
          csv = data.map(row => header.map(fieldName =>
            (fieldName === 'nriga' ||
              fieldName === 'esito' ||
              fieldName === 'cod_err' ||
              fieldName === 'desc_err'
            ) ? isNil(row[fieldName])? '':row[fieldName] :
            isNil(row.campi[fieldName])? '':row.campi[fieldName]
          ).join(','));
          break;
        }
      default:
        header = this.displayedColumns;
        csv = data.map(row => header.map(fieldName =>
          (fieldName === 'nriga' ||
            fieldName === 'esito' ||
            fieldName === 'cod_err' ||
            fieldName === 'desc_err'
          ) ? isNil(row[fieldName])? '':row[fieldName] :
          isNil(row.campi[fieldName])? '':row.campi[fieldName]
        ).join(','));
        break;
    }

    csv.unshift(header.join(','));
    let csvArray = csv.join('\r\n');

    var blob = new Blob([csvArray], { type: 'text/csv' })
    saveAs(blob, "myFile.csv");
  }
}
