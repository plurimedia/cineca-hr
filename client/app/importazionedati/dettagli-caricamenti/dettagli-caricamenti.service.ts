import { Injectable } from '@angular/core';

import { HttpClient } from '@angular/common/http';
import { DataService } from '../shared_services/data.service';


@Injectable({
  providedIn: 'root'
})
export class DettagliCaricamentiService {

  baseURL;

  constructor(
    private http: HttpClient,
    public dataService: DataService) {
      this.baseURL = dataService.baseUrl; // 'http://localhost:3000/api/';
    }

  getElencoDettagliSingoloCaricamento(elementId : string) : any{
    return this.http.get(this.baseURL+"dettagliocaricamentilista/"+elementId); 
  }
}
