import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { NuovoCaricamentoComponent } from './nuovo-caricamento.component';

describe('NuovoCaricamentoComponent', () => {
  let component: NuovoCaricamentoComponent;
  let fixture: ComponentFixture<NuovoCaricamentoComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ NuovoCaricamentoComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(NuovoCaricamentoComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
