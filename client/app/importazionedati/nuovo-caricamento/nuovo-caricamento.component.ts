import { Component, OnInit, ViewChild, Inject } from '@angular/core';
import { FormControl, FormGroupDirective, NgForm } from '@angular/forms';
import { MatSort } from '@angular/material/sort';
import { MatPaginator } from '@angular/material/paginator';
import { MatTableDataSource } from '@angular/material/table';
import { NuovoCaricamentoService } from './nuovo-caricamento.service'
import { ElencoConfigurazioni } from '../models/elencoconfigurazioni.model'
import { isNil, isEqual } from 'lodash';
import { ErrorStateMatcher } from '@angular/material/core';
import { ElencoCaricamenti } from '../models/elencocaricamenti.model';
import { DataService } from '../shared_services/data.service';
import { MatSnackBar } from '@angular/material/snack-bar';
import { MatDialog, MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { saveAs } from 'file-saver';


class InputErrorStateMatcher implements ErrorStateMatcher {
  constructor(private errorstate: boolean) { }
  isErrorState(control: FormControl | null, form: FormGroupDirective | NgForm | null): boolean {
    return this.errorstate;
  }
}

export interface DialogData {
  calendario: Date;
  ore: number;
  minuti: number;
}


@Component({
  selector: 'calendar-dialog',
  templateUrl: 'calendar-dialog.html',
  styleUrls: ['./calendar-dialog.css']
})
export class CalendarDialog {

  constructor(
    public dialogRef: MatDialogRef<CalendarDialog>,
    @Inject(MAT_DIALOG_DATA) public data: DialogData) { }

  onNoClick(): void {
    this.dialogRef.close();
  }

}



@Component({
  selector: 'app-nuovo-caricamento',
  templateUrl: './nuovo-caricamento.component.html',
  styleUrls: ['./nuovo-caricamento.component.css']
})
export class NuovoCaricamentoComponent implements OnInit {
  dataSource;
  datiDaCaricare;
  updata;
  tipologiaList: ElencoConfigurazioni[];
  tipologiaSelected;
  codiceSelected: string;
  condizioneCodice = false;
  fileContent: string = '';
  displayedColumns;
  dynamicColumns;
  ctrlTipologia = new FormControl();
  ctrlCodice = new FormControl();
  ctrlDescrizione = new FormControl();
  ctrlFileChooser = new FormControl();
  matcher: InputErrorStateMatcher;
  descrizioneSelected: any;
  calendario: Date;
  ore: number;
  minuti: number;
  isHidden = true;
  isInAnteprima = false;
  isLoading = true;


  @ViewChild(MatSort, { static: true }) sort: MatSort;
  @ViewChild(MatPaginator, { static: false }) paginator: MatPaginator;


  constructor(public nuovoCaricamentoService: NuovoCaricamentoService,
    public dataService: DataService,
    private _snackBar: MatSnackBar,
    public dialog: MatDialog) { }

  ngOnInit() {

    this.getTipologia()
  }

  //restituisce la lista di Tipologie dal db
  public getTipologia() {
    this.nuovoCaricamentoService.getTipologiaList().subscribe(
      (data: ElencoConfigurazioni[]) => {
        this.tipologiaList = data;
      }
    );
  }

  //controllo dell'unicità del campo Codice
  public checkCodice() {
    this.condizioneCodice = false;
    this.nuovoCaricamentoService.checkCodice(this.codiceSelected).subscribe(
      (data) => {
        if (isNil(data)) {
          this.condizioneCodice = false;
        }
        else if ((data || []).length === 0)
          this.condizioneCodice = false;
        else
          this.condizioneCodice = true;
        this.matcher = new InputErrorStateMatcher(this.condizioneCodice);
      }
    );
  }

  //lettura del file scelto dal chooser
  public pickFile(fileList: FileList): void {
    let file = fileList[0];
    let fileReader: FileReader = new FileReader();
    let self = this;
    fileReader.onloadend = function (x) {
      self.fileContent = fileReader.result as string;
      self.updata = self.fileContent.split('\r\n')
    }
    fileReader.readAsText(file);
  }

  //anteprima file
  public openAnteprima() {
    let fileColumn;
    let configColumn;
    this.isLoading = true;

    if (isNil(this.updata)) {
      this.isHidden = false
    }
    else {
      fileColumn = this.updata[0].split(",");
      this.nuovoCaricamentoService.checkColumnsConfiguration(this.tipologiaSelected).subscribe(
        (data: ElencoConfigurazioni) => {
          configColumn = Object.keys(data[0].campi)

          if (isEqual(fileColumn, configColumn)) {
            this.getTable()
            this.isInAnteprima = true;
            this.ctrlCodice.disable();
            this.ctrlDescrizione.disable();
            this.ctrlTipologia.disable();
            this.ctrlFileChooser.disable();
            this.isHidden = true;
          }
          else
            this.isHidden = false

        })
    }
  }

  //download del file di esempio per verificare il formato
  public getDemoFile() {
    let csv = [];
    var type = this.tipologiaSelected

    this.nuovoCaricamentoService.checkColumnsConfiguration(type).subscribe(
      (data: ElencoConfigurazioni) => {
        let fieldsData = data[0].campi;
        let header = Object.keys(fieldsData);

        let row = []

        for (let i = 0; i < header.length; i++) {
          let key = header[i];

          switch (fieldsData[key]) {
            case "String":
              row[key] = "exampleString";
              break;
            case "Number":
              row[key] = 123.456;
              break;
            case "Boolean":
              row[key] = true;
              break;
            case "Date":
              let today = new Date()
              row[key] = today.getDate() + "/" + (today.getMonth() + 1) + "/" + today.getFullYear() + " 00:00";
              break;
            default:
              row[key] = "any";
              break;
          }

        }

        let values = Object.values(row);

        csv.push(header.join(','));
        csv.push(values.join(','));

        let csvArray = csv.join('\r\n');

        var blob = new Blob([csvArray], { type: 'text/csv' })
        saveAs(blob, "exampleFile.csv");
      })
  }

  //recupera dati dal db e li mostra in tabella
  private getTable() {
    var result = [];
    this.displayedColumns = this.updata[0].split(",");
    this.displayedColumns.unshift("nriga");
    this.dynamicColumns = this.displayedColumns

    for (var i = 1; i < this.updata.length; i++) {
      var obj = {};
      var currentline = this.updata[i].split(",");
      obj["nriga"] = i;
      for (var j = 1; j < this.displayedColumns.length; j++) {
        obj[this.displayedColumns[j]] = currentline[j - 1];
      }
      result.push(obj);
    }
    this.datiDaCaricare = result;

    this.dataSource = new MatTableDataSource(result);
    this.dataSource.sort = this.sort;
    this.dataSource.paginator = this.paginator;
    setTimeout(() => this.isLoading = false, 1000);
  }

  //resetta la gui
  public reset() {
    this.dataSource = new MatTableDataSource([]);
    this.dataSource.sort = this.sort;
    this.dataSource.paginator = this.paginator;
    this.updata = null;
    this.tipologiaSelected = null;
    this.ctrlCodice.reset();
    this.ctrlDescrizione.reset();
    this.ctrlFileChooser.reset();
    this.descrizioneSelected = null;
    this.condizioneCodice = null;
    this.displayedColumns = null;
    this.dynamicColumns = null;
    this.isInAnteprima = false;
    this.ctrlCodice.enable();
    this.ctrlDescrizione.enable();
    this.ctrlTipologia.enable();
    this.ctrlFileChooser.enable();
  }


  //inserisce i dati del file nel db
  public carica(dataSchedulazione: Date) {
    console.log("inizio caricamento");

    let tempStartDate: Date;
    let tempCronDate: Date;
    let tempStatus: string;

    if (isNil(dataSchedulazione)) {
      tempStartDate = new Date();
      tempCronDate = null;
      tempStatus = "In Esecuzione";
    }
    else {
      tempStartDate = null;
      tempCronDate = dataSchedulazione;
      tempStatus = "Schedulato";
    }

    let padreObject: ElencoCaricamenti = {
      tipologia: this.tipologiaSelected,
      operazione: "Inserimento",
      codice: this.codiceSelected,
      descrizione: this.descrizioneSelected,
      start_exec: tempStartDate,
      end_exec: null,
      cron_exec: tempCronDate,
      totale: this.datiDaCaricare.length,
      totale_ok: 0,
      totale_ko: 0,
      status: tempStatus,
      tenant: "",
    }
    console.log(padreObject)
    let listaFigli = this.datiDaCaricare


    if (isNil(dataSchedulazione)) {
      this.nuovoCaricamentoService.caricaEinvia(padreObject, listaFigli).subscribe(
        (data) => {
          this._snackBar.open(
            "Caricamento in esecuzione",
            "x", {
              duration: 2000,
            });
          this.nuovoCaricamentoService.gotoHome();
          console.log("Completato tutto")
        }
      );
    }
    else {
      let dataPadre;
      this.nuovoCaricamentoService.carica(padreObject, listaFigli).subscribe(
        (data) => {
          this._snackBar.open(
            "Caricamento schedulato",
            "x", {
              duration: 2000,
            });
          this.nuovoCaricamentoService.gotoHome();
          console.log("Caricamento effettuato")
          dataPadre = data;

          this.nuovoCaricamentoService.invia(dataPadre, listaFigli, dataSchedulazione).subscribe(
            (data) => {
              console.log("Completato tutto")
            }
          );
        }

      );
    }
  }

//apre la dialog della schedulazione
  public openCalendarDialog(): void {
    const dialogRef = this.dialog.open(CalendarDialog, {
      //width: '500px',
      data: { calendario: this.calendario, ore: this.ore, minuti: this.minuti }
    });

    dialogRef.afterClosed().subscribe(result => {
      console.log('The dialog was closed');
      console.log(result);
      this.calendario = result.calendario;
      this.ore = result.ore;
      this.minuti = result.minuti;

      if (this.calendario && this.ore && this.minuti) {
        this.calendario.setHours(this.ore);
        this.calendario.setMinutes(this.minuti);
        this.calendario.setSeconds(0);
        console.log(this.calendario)
        this.carica(this.calendario)
      }
    });
  }

}
