import { Injectable } from '@angular/core';
import { HttpClient, HttpParams, HttpHeaders } from '@angular/common/http';
import { Router } from '@angular/router';

import { isNil } from 'lodash';
import { throwError } from 'rxjs/internal/observable/throwError';
import { DataService } from '../shared_services/data.service';


@Injectable({
  providedIn: 'root'
})
export class NuovoCaricamentoService {


  baseURL;

  constructor(private http: HttpClient,
    private router: Router,
    public dataService: DataService) {
      this.baseURL = dataService.baseUrl; // 'http://localhost:3000/api/';
    }

  // Http Headers
  httpOptions = {
    headers: new HttpHeaders({
      'Content-Type': 'application/json'
    })
  }

  getTipologiaList(): any {
    return this.http.get(this.baseURL + "elencoconfigurazionilista");
  }

  checkCodice(codice: string): any {

    let params = new HttpParams();

    if (!isNil(codice))
      params = isNil(codice) ? params : params.append('codice', codice);

    return this.http.get(this.baseURL + "elencocaricamentilistasearch", { params: params });
  }

  checkColumnsConfiguration(configurationName: string) {
    return this.http.get(this.baseURL + "elencoconfigurazionilista/" + configurationName);
  }

  caricaEinvia(objPadre: any, elencoRighe: any) {
   
    return this.http.post(this.baseURL + 'caricaeinvia',
      [{ datipadre: objPadre, elenco: elencoRighe }],
      { responseType: 'text' })
  }

  carica(objPadre: any, elencoRighe: any) {
    return this.http.post(this.baseURL + 'carica',
      [{ datipadre: objPadre, elenco: elencoRighe }],
      { responseType: 'text' })
  }

  invia(objPadre: any, elencoRighe: any, parameter: Date){
    let temp: string
    temp = isNil(parameter) ? "" : parameter.toISOString();
    return this.http.post(this.baseURL + 'invia',
      [{ datipadre: objPadre, elenco: elencoRighe, crondate: temp }],
      { responseType: 'text' })
  }
 

  gotoHome() {
    this.router.navigate(['/']);
  }

  errorHandl(error) {
    let errorMessage = '';
    if (error.error instanceof ErrorEvent) {
      // Get client-side error
      errorMessage = error.error.message;
    } else {
      // Get server-side error
      errorMessage = `Error Code: ${error.status}\nMessage: ${error.message}`;
    }
    console.log(errorMessage);
    return throwError(errorMessage);
  }
}
