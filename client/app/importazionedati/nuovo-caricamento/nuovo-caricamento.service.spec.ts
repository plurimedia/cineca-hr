import { TestBed } from '@angular/core/testing';

import { NuovoCaricamentoService } from './nuovo-caricamento.service';

describe('NuovoCaricamentoService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: NuovoCaricamentoService = TestBed.get(NuovoCaricamentoService);
    expect(service).toBeTruthy();
  });
});
