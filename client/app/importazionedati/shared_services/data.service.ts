import { Injectable } from '@angular/core';


//classe per condivisione dati tra componenti
@Injectable({
  providedIn: 'root'
})
export class DataService{

  public data: any;
  public baseUrl: string;

  constructor() { 
    this.baseUrl= 'http://localhost:3000/api/'; //cambiare qui indirizo client facendo attenzione a lasciare /api/
  }
}
