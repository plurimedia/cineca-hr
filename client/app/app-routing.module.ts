import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';


import { TableCaricamentiComponent } from './importazionedati/table-caricamenti/table-caricamenti.component';
import { NuovoCaricamentoComponent } from './importazionedati/nuovo-caricamento/nuovo-caricamento.component';
import { DettagliCaricamentiComponent } from './importazionedati/dettagli-caricamenti/dettagli-caricamenti.component';

const routes: Routes = [
  {
    path: '',
    component: TableCaricamentiComponent
  },
  {
    path: 'new',
    component: NuovoCaricamentoComponent
  },
  {
    path: 'dettagli',
    component: DettagliCaricamentiComponent
  }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
