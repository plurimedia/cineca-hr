import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { HttpClientModule } from '@angular/common/http';

import {BrowserAnimationsModule} from '@angular/platform-browser/animations';
import {FormsModule, ReactiveFormsModule} from '@angular/forms';
import {MatNativeDateModule} from '@angular/material/core';
import {MatSelectModule} from '@angular/material/select';
import {MatDatepickerModule} from '@angular/material/datepicker';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';

import { MaterialModule } from '../material-module';
import {MAT_DATE_LOCALE} from '@angular/material/core';

import { TableCaricamentiComponent, DeleteDialog } from './importazionedati/table-caricamenti/table-caricamenti.component';
import { NuovoCaricamentoComponent, CalendarDialog } from './importazionedati/nuovo-caricamento/nuovo-caricamento.component';
import { DettagliCaricamentiComponent } from './importazionedati/dettagli-caricamenti/dettagli-caricamenti.component';

import {TableCaricamentiService} from './importazionedati/table-caricamenti/table-caricamenti.service'
import { NuovoCaricamentoService } from './importazionedati/nuovo-caricamento/nuovo-caricamento.service';
import { DettagliCaricamentiService } from './importazionedati/dettagli-caricamenti/dettagli-caricamenti.service';

@NgModule({
  declarations: [
    AppComponent,
    TableCaricamentiComponent,
    DeleteDialog,
    NuovoCaricamentoComponent,
    CalendarDialog,
    DettagliCaricamentiComponent
  ],
  imports: [
    BrowserModule,
    HttpClientModule,
    AppRoutingModule,
    BrowserAnimationsModule,
    FormsModule,
    ReactiveFormsModule,
    MaterialModule,
    MatNativeDateModule,
    MatSelectModule,
    MatDatepickerModule
  ],
  entryComponents: [CalendarDialog, DeleteDialog],
  providers: [
    {provide: MAT_DATE_LOCALE, useValue: 'en-GB'},
    TableCaricamentiService,
    NuovoCaricamentoService,
    DettagliCaricamentiService
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
