const ElencoCaricamenti = require('../models/elencocaricamenti.model');
const DettaglioCaricamenti = require('../models/dettagliocaricamenti.model');
const ElencoConfigurazioni = require('../models/elencoconfigurazioni.model');

const _ = require("lodash");
const axios = require("axios");
var CONFIG = require(process.cwd() + '/server/config.js');
var cron = require('node-cron');

//carica l'oggetto padre nel db, entry point
function caricaPadre(body) {
    body.datipadre.tenant = CONFIG.TENANT;
    return new Promise((resolve, reject) => {
        let newCaricamento = new ElencoCaricamenti(body.datipadre);
        console.log(newCaricamento)
        newCaricamento.save()
            .then((caricamento) => {
                console.log('padre ok')
                resolve({
                    objpadre: caricamento,
                    elencoFigli: body.elenco
                })
            })
            .catch((err) => {
                console.log('padre err', err)
                reject(err)
            })
    })
}

//carica tutti i figli (righe del file) relativi al padre
function caricaFigli(rispPadre) {
    return new Promise((resolve, reject) => {

        let listaFigli = rispPadre.elencoFigli;
        let padre = rispPadre.objpadre;
        let idPadre = padre.id;
        let dataDettagliObject = []

        for (var i = 0; i < listaFigli.length; i++) {

            let fields = _.omit(listaFigli[i], 'nriga');

            let dettaglioObject = {
                caricamento: idPadre,
                nriga: listaFigli[i]["nriga"],
                campi: fields,
                esito: null,
                codice_errore: null,
                descrizione_errore: null,
                tenant: CONFIG.TENANT
            }
            dataDettagliObject.push(dettaglioObject);
        }

        var saved = 0
        dataDettagliObject.forEach(async (element, i) => {
            try {

                let temp = new DettaglioCaricamenti(element);
                await temp.save()
                saved++
                if (saved === dataDettagliObject.length) {
                    console.log('finito tutte le righe')
                    resolve({ parent: padre, elenco: dataDettagliObject })
                }
            } catch (err) {
                console.log('errore salvataggio righe')
                reject(err)
            }
        });
    })
}

/* 
** api richiamata dal client
** funzione che si occupa solo del caricamento dei dati sul db (carica sia padre che figli)
** al client restituisce l'id padre, al server l'oggetto da inviare
*/
function carica(req, res) {
    return new Promise((resolve, reject) => {
        caricaPadre(req.body[0])
            .then(caricaFigli)
            .then((risp) => {

                resolve(risp)
                res.send(risp.parent)

            })
            .catch((err) => {
                res.status(500).send('Errore in fase di caricamento sul db locale')
            })
    });
}

//api richiamata dal client, carica i dati sul db locale e invia immediatamente i dati sul server esterno
function caricaeinvia(req, res) {
    carica(req, res)
        .then(invioDati)
        .then((risp) => {
            console.log("Caricamento e invio completati");
            res.end();//sarebbe res 200
        })
        .catch((err) => {
            res.status(500).send('aiuto')
        })
}


//api richiamata dal client, invio dei dati con esecuzione schedulata
function invia(req, res) {
    res.sendStatus(200) //il client vede che ha finito, ma continua il lavoro in background

    let startdate = new Date(req.body[0].crondate);
    let parent = JSON.parse(req.body[0].datipadre);
    let elenco = req.body[0].elenco;

    let obj = {
        parent: parent,
        elenco: elenco
    }

    let crontstring = startdate.getMinutes() + ' '
        + startdate.getHours() + ' '
        + startdate.getUTCDate() + ' '
        + (startdate.getUTCMonth() + 1) + ' *';

    cron.schedule(crontstring, async () => {
        updateStartDate(obj)
            .then(invioDati)
            .then()
    });

}

/* 
** invio dati al server esterno, i dati vengono suddivisi in pacchetti 
** la grandezza dei pacchetti viene definita nel file config.js (PAGE_SIZE)
** ogni pacchetto viene spedito e dopo aver ricevuto una risposta dal server esterno
** viene effettuato un update sul db locale
*/
function invioDati(obj) {
    return new Promise((resolve, reject) => {

        let parent = obj.parent;
        let elenco = obj.elenco;

        let pagesize = CONFIG.PAGE_SIZE;
        let elencosize = elenco.length;
        let page_num = _.floor(elencosize / pagesize)
        let skips = 0

        let index = 0
        while (index <= page_num) {

            skips = index * pagesize;

            trovaChunk(parent, skips, pagesize)
                .then(mandaChunk)
                .then(updateDettagli)
                .then((risp) => { })

            index++;
        }
        resolve();
    });
}

//trova il chunk dei dati da inviare
function trovaChunk(parent, skips, limit) {
    let idparent = parent._id;
    return new Promise((resolve, reject) => {
        DettaglioCaricamenti.find({
            caricamento: idparent
        })
            .skip(skips)
            .limit(limit)
            .exec()
            .then((risp) => {
                risp['parent'] = parent;
                resolve(risp)
            })
            .catch((err) => {
                reject(err)
            })
    })
}

//manda il chunk individuato (connessione API Cineca)
function mandaChunk(dati) {
    console.log(dati.parent.tipologia);
    return new Promise((resolve, reject) => {

        getInserimentoAPI(dati)
            .then(callInserimentoAPI)
            .then((risp) => {
                console.log(risp)
                resolve(risp) 
            }) 
    });
}

//prende l'api esterna di inserimento
function getInserimentoAPI(dati) {
    let nomeT = dati.parent.tipologia;
    
    return new Promise((resolve, reject) => {
        ElencoConfigurazioni.find({
            nome: nomeT
        })
        .exec()
        .then(configurazione => {
            console.log(configurazione);
            resolve({
                api: configurazione[0].api.inserimento,
                dati: dati
            })
        })
        .catch((err) => {
            reject(err)
        });
    });
}


//prende l'api esterna di inserimento
function callInserimentoAPI(obj) {
    let iAPI = obj.api; //stringa api
    let parent = obj.dati.parent; //conservo riferimenti parent
    let objToSend = _.remove(obj.dati, ()=> obj.dati['parent']); //senza parent

    for (let i = 0; i < objToSend.length; i++) {

        objToSend[i].esito = "ok"  //cambiare qui - fittizio rimuovere tutto il ciclo for ********************

     }

    return new Promise((resolve, reject) => {
      
         resolve({objToSend,parent}) // cambiare qui - togliere dopo inserimento api in db *******************

     /*    axios.post(iAPI, objToSend)  //cambiare qui - attivare dopo inserimento api in db *****************
        .then(response => {
            console.log(response);
            resolve({response,parent});
        })
        .catch((err) => {
            reject(err)
        }); */
    });
}


/* 
** update del singolo oggetto DettaglioCaricamenti di ritorno da cineca 
** tale oggetto conterrà eventuali elementi in più relativi alle informazioni sugli errori
*/
function singleUpdate(dato) {
    return new Promise((resolve, reject) => {
        DettaglioCaricamenti.findByIdAndUpdate(
            dato.id,
            dato,
            {
                new: true,
                upsert: true
            })
            .exec()
            .then((risp) => {
                resolve(risp)
            })
            .catch((err) => {
                reject(err)
            })
    });
}

//Cerca tutti gli elementi con esito "ok"
function elencoFigliOk(obj) {
  //  console.log(obj);
    return new Promise((resolve, reject) => {
        DettaglioCaricamenti.find({
            caricamento: obj.dati.parent,
            esito: "ok"
        })
            .exec()
            .then((risp) => {
                obj['datiOk'] = risp
                resolve(obj)
            })
            .catch((err) => {
                reject(err)
            })
    })
}

//Cerca tutti gli elementi con esito "ko"
function elencoFigliKo(obj) {
  //  console.log(obj);
    return new Promise((resolve, reject) => {
        DettaglioCaricamenti.find({
            caricamento: obj.dati.parent, 
            esito: "ko"
        })
            .exec()
            .then((risp) => {
                obj['datiKo'] = risp
                resolve(obj)
            })
            .catch((err) => {
                reject(err)
            })
    })
}

//update dei campi totali
function updateTotali(obj) {
 //   console.log(obj);
    let id = obj.dati.parent; 
    let n_ok = obj.datiOk.length;
    let n_ko = obj.datiKo.length;

    return new Promise((resolve, reject) => {
        ElencoCaricamenti.findByIdAndUpdate(
            id,
            {
                $set: {
                    totale_ok: n_ok,
                    totale_ko: n_ko
                }
            },
            {
                new: true,
                upsert: true
            }
        )
            .exec()
            .then((risp) => {
                obj['padre'] = risp
                resolve(obj)
            })
            .catch((err) => {
                reject(err)
            });
    });
}

//funzione che effettua un update su Stato e Fine esecuzione del padre (ElencoCaricamenti)
function updateDefinitivo(obj) {
  //  console.log(obj);
    let id = obj.padre.id; 
    let n_ko = obj.datiKo.length;
    let n_ok = obj.datiOk.length;
    let nrows = obj.padre.totale;
    let islast = (n_ko + n_ok === nrows) ? true : false;


    let statusString = (n_ko === 0) ? "Finito" : "In Errore";

    return new Promise((resolve, reject) => {

        if (islast) {
            ElencoCaricamenti.findByIdAndUpdate(
                id,
                {
                    $set: {
                        status: statusString,
                        end_exec: new Date()
                    }
                },
                {
                    new: true,
                    upsert: true
                })
                .exec()
                .then((r) => {
                    resolve(r)
                })
                .catch((err) => {
                    reject(err)
                });
        }
    });


}

//update di tutti i figli (DettaglioCaricamenti)
function updateDettagli(dati) {
console.log(dati)
    let objResp = dati.objToSend

    return new Promise((resolve, reject) => {

        for (let i = 0; i < objResp.length; i++) {
            //update del singolo elemento
            singleUpdate(objResp[i]).then()
        }

        let obj = { dati:dati }

        //check numerosità ok/ko + update totali (da eseguire dopo update del chunk)
        elencoFigliOk(obj)
            .then(elencoFigliKo)
            .then(updateTotali)
            .then(updateDefinitivo)
            .catch((err) => {
                reject(err)
            });


    });
}

//update della startdate
function updateStartDate(obj) {

    let id = obj.parent._id; 

    return new Promise((resolve, reject) => {
        ElencoCaricamenti.findByIdAndUpdate(
            id,
            {
                $set: {
                    start_exec: new Date()
                }
            },
            {
                new: true,
                upsert: true
            }
        )
            .exec()
            .then((risp) => {
                //obj['padre'] = risp
                resolve(obj)
            })
            .catch((err) => {
                reject(err)
            });
    });
}


/*
------------------------------------------------------
    Elimina Caricamento
------------------------------------------------------
*/

//api richiamata dal client
function elimina(req, res) {
    let obj = req.body;

    inCancellazione(obj)
        .then(getEliminaAPI)
        .then(callEliminaPI)
        .then(insertCaricamento)
        .then(logicalDeleteCaricamento)
        .then(
            (elem) => {
                res.send({
                    success: true,
                    message: 'Cancellazione logica avvenuta con successo'
                })
            }  
        )
}

//effettua un update sullo stato, imposta "In Cancellazione" quando parte il processo di eliminazione
function inErrore(req){
    let id = req._id
    return new Promise((resolve, reject) => {
        ElencoCaricamenti.findByIdAndUpdate(
            id,
            {
                status: "Errore"
            },
            {
                new: true,
                upsert: true 
            }
        )
            .exec()
            .then(
                resolve(req)
            )
            .catch((err) => {
                reject(err)
            });
    });

}

//effettua un update sullo stato, imposta "In Cancellazione" quando parte il processo di eliminazione
function inCancellazione(req){
    let id = req._id
    return new Promise((resolve, reject) => {
        ElencoCaricamenti.findByIdAndUpdate(
            id,
            {
                status: "In Cancellazione"
            },
            {
                new: true,
                upsert: true 
            }
        )
            .exec()
            .then(
                resolve(req)
            )
            .catch((err) => {
                reject(err)
            });
    });

}

//recupero api esterna di cancellazione
function getEliminaAPI(dati) {
    let nomeT = dati.tipologia;
    return new Promise((resolve, reject) => {
        ElencoConfigurazioni.find({
            nome: nomeT
        })
            .exec()
            .then(configurazione => {
                resolve({
                    api: configurazione[0].api.cancellazione, //prendo solo cancellazione
                    dati: dati
                } )
            })
            .catch((err) => {
                reject(err)
            });
    });
}

//prende l'api esterna di cancellazione
function callEliminaPI(obj) {
    let iAPI = obj.api; //stringa api
    let objToSend = obj.dati; 

    return new Promise((resolve, reject) => {
        
         resolve(objToSend) // cambiare qui - togliere dopo inserimento della cancellazione api in db *******************

     /*    axios.post(iAPI, objToSend)  //cambiare qui - attivare dopo inserimento della cancellazione api in db *****************
        .then(response => {
            console.log(response);
            resolve(response);
        })
        .catch((err) => {
            inErrore(obj)
            reject(err)

        }); */
    });
}

//duplica la riga da cancellare e fa un nuovo inserimento nel db, impostando però in alcuni campi il prefix CANC_
function insertCaricamento(req){
    let newCaricamento = new ElencoCaricamenti(req);
    newCaricamento.operazione = "Cancellazione";
    newCaricamento.codice = "CANC_" + req.codice;
    newCaricamento.descrizione = "CANC_" + req.descrizione;
    newCaricamento.status = "Cancellato";
    newCaricamento.start_exec = new Date();
    newCaricamento.end_exec = new Date();
   // newCaricamento.rif = req._id;
    newCaricamento._id = null
    newCaricamento.mdate = new Date();
            
    return new Promise((resolve, reject) => {

        newCaricamento.save()
            .then((caricamento) => {
                console.log('padre canc ok')
                resolve(req)
            })
            .catch((err) => {
                console.log('padre canc err', err)
                reject(err)
            })
    })
}

//update di status e operazione dell'elemento cancellato
function logicalDeleteCaricamento(req) {

    let id = req._id
    return new Promise((resolve, reject) => {
        ElencoCaricamenti.findByIdAndUpdate(
            id,
            {
                operazione: "Cancellazione",
                status: "Cancellato"
            },
            {
                new: true,
                upsert: true 
            }
        )
            .exec()
            .then(
                resolve()
            )
            .catch((err) => {
                reject(err)
            });
    });

}

module.exports = { caricaeinvia, carica, invia, elimina };