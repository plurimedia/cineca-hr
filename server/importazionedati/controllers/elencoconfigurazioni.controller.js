const ElencoConfigurazioni = require('../models/elencoconfigurazioni.model');


function validationError(res, statusCode) {
  statusCode = statusCode || 422;
  return function (err) {
    return res.status(statusCode).json(err);
  };
}

function handleError(res, statusCode) {
  statusCode = statusCode || 500;
  return function (err) {
    return res.status(statusCode).send(err);
  };
}

function getElencoConfigurazioni(req, res) {
  return ElencoConfigurazioni.find() 
    .exec() 
    .then(configurazioni => { 
      res.status(200).json(configurazioni);
    })
    .catch(handleError(res)); 
}

function getConfigurazione(req, res) {
  let param = req.params.id
  return ElencoConfigurazioni.find({
    nome : param
  }) 
  .exec() 
  .then(configurazione => { 
    res.status(200).json(configurazione);
  })
  .catch(handleError(res));
}


function createConfigurazione(req, res) {
  let newConfigurazione = new ElencoConfigurazioni(req.body);
  return newConfigurazione.save().then(function(configurazione) { 
    res.json({configurazione}); 
  })
  .catch(validationError(res)); 
}

module.exports = { getElencoConfigurazioni, getConfigurazione, createConfigurazione};