const DettaglioCaricamenti = require('../models/dettagliocaricamenti.model');

function handleError(res, statusCode) {
  statusCode = statusCode || 500;
  return function (err) {
    return res.status(statusCode).send(err);
  };
}

function findListaDettaglioCaricamentiByParentId(req, res) {
  let idparent = req.params.idparent
  return DettaglioCaricamenti.find({
    caricamento: idparent
  })
    .sort({ nriga: 1 })
    .exec()
    .then(dettaglio => {
      res.status(200).json(dettaglio);
    })
    .catch(handleError(res));
}

module.exports = { findListaDettaglioCaricamentiByParentId };
