const ElencoCaricamenti = require('../models/elencocaricamenti.model');


function handleError(res, statusCode) {
  statusCode = statusCode || 500;
  return function (err) {
    return res.status(statusCode).send(err);
  };
}

function getElencoCaricamenti(req, res) {
  return ElencoCaricamenti.find()
    .sort({ mdate: -1 })
    .exec()
    .then(caricamenti => {
      res.status(200).json(caricamenti);
    })
    .catch(handleError(res));
}

function findCaricamento(req, res) {
  let id = req.params.id

  return ElencoCaricamenti.find(
    { _id: id }
    )
    .exec()
    .then(elemento => {
      res.status(200).json(elemento);
    })
    .catch(handleError(res));
}

function getElencoCaricamentiSearch(req, res, next) {
  let conditions = req.query;

  if (conditions.codice)
    conditions.codice = { '$regex': conditions.codice }
  if (conditions.descrizione)
    conditions.descrizione = { '$regex': conditions.descrizione }
  if (conditions.start_exec)
    conditions.start_exec = { "$gte": new Date(conditions.start_exec) }
  if (conditions.end_exec)
    conditions.end_exec = { "$lte": new Date(conditions.end_exec) }
  if (conditions.cron_exec)
    conditions.cron_exec = { "$eq": new Date(conditions.cron_exec) }

  return ElencoCaricamenti.find(conditions)
    .sort({ mdate: -1 })
    .exec()
    .then(caricamenti => {
      res.status(200).json(caricamenti);
    })
    .catch(handleError(res));
}


module.exports = { getElencoCaricamenti, getElencoCaricamentiSearch };