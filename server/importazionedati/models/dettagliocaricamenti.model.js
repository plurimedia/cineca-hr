const mongoose = require('mongoose');
mongoose.Promise = require('bluebird');
mongoose.set('useFindAndModify', false);
const Schema = mongoose.Schema;

const DettaglioCaricamentiSchema = new Schema({
  caricamento: {
    type: Schema.Types.ObjectId, ref: 'ElencoCaricamenti'
  },
  nriga: {
    type: Number,
    required: true
  },
  campi: {
    type: Schema.Types.Mixed
  },
  esito: {
    type: String,
    required: false
  },
  cod_err: {
    type: String,
    required: false
  },
  desc_err: {
    type: String,
    required: false
  },
  mdate: {
    type: Date,
    default: Date.now
    }
});

module.exports = mongoose.model('DettaglioCaricamenti', DettaglioCaricamentiSchema, 'dettagliocaricamenti');