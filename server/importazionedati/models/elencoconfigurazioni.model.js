const mongoose = require('mongoose');
mongoose.Promise = require('bluebird');
const Schema = mongoose.Schema;

const ElencoConfigurazioniSchema = new Schema({
  nome: {
    type: String,
    required: true
  },
  campi: {
    type: Schema.Types.Mixed
  },
  api: {
    type: Schema.Types.Mixed
  },
  tenant: {
    type: String,
    required: true
  },
  mdate: {
    type: Date,
    default: Date.now
    }
});

module.exports = mongoose.model('ElencoConfigurazioni', ElencoConfigurazioniSchema, 'elencoconfigurazioni');