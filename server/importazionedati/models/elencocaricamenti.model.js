const mongoose = require('mongoose');
mongoose.Promise = require('bluebird');
const Schema = mongoose.Schema;

const ElencoCaricamentiSchema = new Schema({
  tipologia: {
    type: String,
    required: true
  },
  operazione: {
    type: String,
    required: true
  },
  codice: {
    type: String,
    required: true
  },
  descrizione: {
    type: String,
    required: true
  },
  start_exec: {
    type: Date,
    required: false
  },
  end_exec: {
    type: Date,
    required: false
  },
  cron_exec: {
    type: Date,
    required: false
  },
  totale: {
    type: Number,
    required: false
  },
  totale_ok: {
    type: Number,
    required: false
  },
  totale_ko: {
    type: Number,
    required: false
  },
  status: {
    type: String,
    required: true
  },
  tenant: {
    type: String,
    required: true
  },
  mdate: {
    type: Date,
    default: Date.now
    }
});

module.exports = mongoose.model('ElencoCaricamenti', ElencoCaricamentiSchema, 'elencocaricamenti');