const express = require('express');

const ElencoConfigurazioniController = require('../controllers/elencoconfigurazioni.controller');

router = express();

router.get('/api/elencoconfigurazionilista', ElencoConfigurazioniController.getElencoConfigurazioni);
router.get('/api/elencoconfigurazionilista/:id', ElencoConfigurazioniController.getConfigurazione);
router.post('/api/elencoconfigurazionilista', ElencoConfigurazioniController.createConfigurazione);
 
module.exports = router;