const express = require('express');

const ElencoCaricamentiController = require('../controllers/elencocaricamenti.controller');

router = express();

router.get('/api/elencocaricamentilista', ElencoCaricamentiController.getElencoCaricamenti);
router.get('/api/elencocaricamentilistasearch', ElencoCaricamentiController.getElencoCaricamentiSearch);
 
module.exports = router;