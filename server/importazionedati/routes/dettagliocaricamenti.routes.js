const express = require('express');

const DettaglioCaricamentiController = require('../controllers/dettagliocaricamenti.controller');

router = express();

router.get('/api/dettagliocaricamentilista/:idparent', DettaglioCaricamentiController.findListaDettaglioCaricamentiByParentId);
 
module.exports = router;