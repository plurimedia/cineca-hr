const express = require('express');
const CrossController = require('../controllers/cross.controller');

router = express();

router.post('/api/caricaeinvia', CrossController.caricaeinvia);
router.post('/api/carica', CrossController.carica);
router.post('/api/invia', CrossController.invia);
router.put('/api/elimina', CrossController.elimina);

module.exports = router;