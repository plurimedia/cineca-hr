var config = {
    production: {
        ENV:'production',
        PORT: 3000,
        DEBUG:'false', 
        MONGO: '',
        PAGE_SIZE: 2,
        TENANT: 'BOCCONI'
    }, 
    test: {
        ENV:'test',
        PORT: 3000,
        DEBUG:'false', 
        MONGO: '',
        PAGE_SIZE: 2,
        TENANT: 'BOCCONI'
        
    },
    dev: {  
        ENV:'dev',
        PORT: 3000,
        DEBUG:'false', 
        MONGO: 'mongodb://localhost:27017/nuovodb',
        PAGE_SIZE: 2,
        TENANT: 'BOCCONI'

    }
}
//cambiare qui sopra i settaggi

_setEnv = function (ambiente) {
   if(ambiente === 'production'){
        return config.production;
    }
    else if(ambiente === 'test'){
        return config.test;
    }
    else{
        return config.dev;
    }
};
 
// dipende dalla variabile di env "ambiente" settato su heroku
module.exports = _setEnv(process.env.AMBIENTE);
